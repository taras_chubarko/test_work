<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class PropertySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = storage_path('app/property-data.csv');//Storage::get('property-data.csv');
        $arr = $this->csvToArray($file);
        foreach ($arr as $item){
            \App\Property::create([
                'name' => $item['Name'],
                'price' => $item['Price'],
                'bedrooms' => $item['Bedrooms'],
                'bathrooms' => $item['Bathrooms'],
                'storeys' => $item['Storeys'],
                'garages' => $item['Garages'],
            ]);
        }
    }

    public function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }
}
