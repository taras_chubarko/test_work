<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;

class PropertyController extends Controller
{
    protected $model;
    /* public function __construct
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function __construct(Property $model)
    {
        $this->model = $model;
    }
    /* public function property
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function property(Request $request)
    {
        $q = $this->model->orderBy('created_at', 'DESC');

        if($request->has('name')){
            $q->where('name', 'like', '%'.$request->name.'%');
        }
        if($request->has('bedrooms') && $request->bedrooms){
            $q->where('bedrooms',  $request->bedrooms);
        }
        if($request->has('price') && $request->price){
            $q->whereBetween('price', $request->price);
        }
        if($request->has('bathrooms') && $request->bathrooms){
            $q->where('bathrooms',  $request->bathrooms);
        }
        if($request->has('storeys') && $request->storeys){
            $q->where('storeys',  $request->storeys);
        }
        if($request->has('garages') && $request->garages){
            $q->where('garages',  $request->garages);
        }
        $data = $q->get();

        return response()->json(['data' => $data], 200);
    }
}
