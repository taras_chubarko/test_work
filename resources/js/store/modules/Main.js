import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import * as types from '../mutation-types'
import {getField, updateField} from 'vuex-map-fields'

Vue.use(Vuex)

const state = {
    apiHost: 'http://127.0.0.1:8000/api/',
    property: [],
    query:{
        price:[263604,572002],
    }
};

const getters = {
    getField
};

const actions = {
    async getData({commit, state}){
        try {
            var str = $.param( state.query )
            const response = await axios.get(state.apiHost+'property?'+str)
            commit(types.SET_PROPERTY, response.data.data)
            return response
        } catch (err) {
            throw err
        }
    }
};

const mutations = {
    updateField,
    [types.SET_PROPERTY](state, data){
        state.property = data
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
